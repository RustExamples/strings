fn main() {
    // String are growable, mutable & UTF-8 encoded collections
    // str (string slices) only string type in Rust's core language
    // "strings" usually refer to String & str

    // Create an empty string
    let mut s = String::new();

    // Create string from initial data
    let data = "initial text";
    let s = data.to_string();

    // Create string directly from literals
    let s = "initial text".to_string();

    // Using `from`
    let s = String::from("initial text");

    let hello = String::from("السلام عليكم");
    let hello = String::from("Dobrý den");
    let hello = String::from("Hello");
    let hello = String::from("שָׁלוֹם");
    let hello = String::from("नमस्ते");
    let hello = String::from("こんにちは");
    let hello = String::from("안녕하세요");
    let hello = String::from("你好");
    let hello = String::from("Olá");
    let hello = String::from("Здравствуйте");
    let hello = String::from("Hola");

    // Updating a string using push_str
    let mut s = String::from("foo");
    // Doesn't take ownership of passed in parameter
    s.push_str("bar");

    // We still might want to use s2 after appending
    let s2 = "baz";
    s.push_str(s2);
    println!("{}", s2);

    // Update using push
    let mut s = String::from("foo");
    s.push('l');
    println!("{}", s);

    // Concatenate string with + operator
    let s1 = String::from("foo");
    let s2 = String::from("bar");
    // s1 is moved here and can no longer be used
    // + operator implements add trait => fn add(self, s: &str) -> String {}
    // &s2 is &String not &str. Rust uses deref coercion to convert
    // &String to &str => &s2[..]
    // + takes ownership of self, so s1 is moved into add
    // &s2 is appended to s1 and returns ownership of result
    let s3 = s1 + &s2;

    // Concatenate string with format! macro
    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    // Similar to println!, but returns string 
    // Doesn't take any ownership
    let s = format!("{}-{}-{}", s1, s2, s3);

    // Indexing a string
    let s1 = String::from("hello");
    // Rust doesn't support indexing like other languages
    // Throws compile time error
    // let h = s1[0];

    // Internal memory representation of string
    // String is a wrapper over Vec<u8>

    // Length is 24 - meaning 2 bytes per UTF-8 character
    let hello = String::from("Здравствуйте");
    println!("len: {}", hello.len());

    // Length is 4 - meaning 4 bytes per UTF-8 character
    let hello = String::from("Hola");
    println!("len: {}", hello.len());

    // So, its not possible to properly index a string

    // On UTF-8 encode, 1st byte - 208, 2nd byte - 151
    let hello = "Здравствуйте";
    // So, &hello[0] != 3 but 208
    // But, its not what user would want and thats only
    // data Rust has at index 0
    // To avoid unexpected bugs, Rust doesn't compile the following code
    // let answer = &hello[0];

    // Relevant ways to look at strings -
    // 1) Bytes 2) Scalar values 3) Graphmeme clusters (closes thing to a letter)

    // नमस्ते => Vec<u8> bytes => [224, 164, 168, 224, 164, 174, 224, 164, 184, 224, 165, 141, 224, 164, 164, 224, 165, 135]
    // नमस्ते => scalar values (char type) => ['न', 'म', 'स',  '्', 'त',  'े']
    // index[3] & index[5] not letters but diacritics (no meaning on their own)
    // नमस्ते => Graphmeme clusters => ["न", "म", "स्", "ते"] (letters as we understand)

    // Indexing is supposed to be O(1), since Rust has to walk through content
    // from begining to find valid characters. So, another reason indexing isn't allowed.

    // Slicing a string
    // Indexing is bad as we don't know if we need 1) Bytes 2) Scalar values
    // 3) Graphmeme clusters or, 4) string slices
    // If a slice is required use range
    let hello = "Здравствуйте";
    // Returns first 4 bytes which "3д"
    let s = &hello[0..4];
    // This would panic at runtime, since character boundary for hello
    // is 2 and we want one byte
    // let s = &hello[0..1];
    // Should be cautious when slicing string
    // It can crash programs

    // Iterating a string with chars method
    for c in "नमस्ते".chars() {
        println!("{}", c);
    }

    // Iterating a string with raw bytes
    for b in "नमस्ते".bytes() {
        println!("{}", b);
    }

    // Unicode characters are made up of more than 1 bytes
    // Getting grapheme clusters in complex
    // not in std lib, use availables crate
}
